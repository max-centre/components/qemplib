cmake_minimum_required(VERSION 3.20 FATAL_ERROR)

set(CMAKE_POLICY_DEFAULT_CMP0048 NEW)

project(qeMPLib 
    VERSION 0.1
    DESCRIPTION "qeMPLib: opEn-Source Package that replicates QE MPI envinronment plus timing and error-logging utilities"
    LANGUAGES Fortran C)
##################################################################################################################
if(${qeMPLib_BINARY_DIR} STREQUAL ${qeMPLib_SOURCE_DIR})
    message(FATAL_ERROR "QE source folder cannot be safely used as a build folder!")
endif()

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${qeMPLib_BINARY_DIR}/lib 
    CACHE
    PATH "Single output directory for building all libraries.")
 
option(QEMPLIB_SUBMODULE_BUILD "builds qemplib as QE submodule" OFF) 



if(NOT QEMPLIB_SUBMODULE_BUILD) 
  set(CMAKE_MODULE_PATH "${CMAKE_CURRENT_SOURCE_DIR}/cmake" ${CMAKE_MODULE_PATH})
  include_directories("${CMAKE_CURRENT_SOURCE_DIR}/include")
  include(cmake/qeHelpers.cmake)
  
  
  option(QE_ENABLE_OPENMP
         "enable distributed execution support via OpenMP" ON)
  
  option(QE_ENABLE_MPI
      "enable distributed execution support via MPI" ON)
  
  
  
  if(QE_ENABLE_MPI)
      qe_add_global_compile_definitions(__MPI OMPI_SKIP_MPICXX)
      if(QE_ENABLE_MPI_GPU_AWARE)
          qe_add_global_compile_definitions(__GPU_MPI)
      endif()
      #
      add_library(qe_mpi_fortran INTERFACE) 
      find_package(MPI REQUIRED Fortran)
      target_link_libraries(qe_mpi_fortran
          INTERFACE MPI::MPI_Fortran)
      message(STATUS "MPI settings used by CTest")
      message("     MPIEXEC_EXECUTABLE : ${MPIEXEC_EXECUTABLE}")
      message("     MPIEXEC_NUMPROC_FLAG : ${MPIEXEC_NUMPROC_FLAG}")
      message("     MPIEXEC_PREFLAGS : ${MPIEXEC_PREFLAGS}")
      string(REPLACE ";" " " MPIEXEC_PREFLAGS_PRINT "${MPIEXEC_PREFLAGS}")
      message("   Tests run as : ${MPIEXEC_EXECUTABLE} ${MPIEXEC_NUMPROC_FLAG} <NUM_PROCS> ${MPIEXEC_PREFLAGS_PRINT} <EXECUTABLE>")
  endif()
  
   
  ###########################################################
  # OpenMP
  # The following targets will be defined:
  add_library(qe_openmp_fortran INTERFACE)
  add_library(qe_openmp_c INTERFACE)
  ###########################################################
  if(QE_ENABLE_OPENMP)
      find_package(OpenMP REQUIRED Fortran C)
      target_link_libraries(qe_openmp_fortran INTERFACE OpenMP::OpenMP_Fortran)
      target_link_libraries(qe_openmp_c INTERFACE OpenMP::OpenMP_C)
  endif(QE_ENABLE_OPENMP)
  if(QE_ENABLE_OFFLOAD)
      target_compile_definitions(qe_openmp_fortran INTERFACE "$<$<COMPILE_LANGUAGE:Fortran>:__OPENMP_GPU>")
  endif()
  
  ###########################################################
  # Load different compiler settings
  ###########################################################
   if(CMAKE_Fortran_COMPILER_ID MATCHES "GNU")
       include(GNUFortranCompiler)
   elseif(CMAKE_Fortran_COMPILER_ID MATCHES "PGI" OR CMAKE_Fortran_COMPILER_ID MATCHES "NVHPC")
       include(NVFortranCompiler)
   elseif(CMAKE_Fortran_COMPILER_ID MATCHES "Cray")
       include(CrayFortranCompiler)
   elseif(CMAKE_Fortran_COMPILER_ID MATCHES "Intel")
       include(IntelFortranCompiler)
   elseif(CMAKE_Fortran_COMPILER_ID MATCHES "XL")
       include(IBMFortranCompiler)
   endif()
  #################################################################


  install (DIRECTORY ${qeMPLib_BINARY_DIR}/lib DESTINATION "./")  
  set(modfiles 
	${qeMPLib_BINARY_DIR}/src/mod/qeMPLib/mp.mod
	${qeMPLib_BINARY_DIR}/src/mod/qeMPLib/mp_wave.mod
        ${qeMPLib_BINARY_DIR}/src/mod/qeMPLib/util_param.mod
        ${qeMPLib_BINARY_DIR}/src/mod/qeMPLib/parallel_include.mod)
  install (FILES  ${modfiles} DESTINATION "./include") 
###########################################################
endif()  

add_subdirectory(src) 



