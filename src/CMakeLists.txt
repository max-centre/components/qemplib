set(src_util
    clocks_handler.f90
    data_buffer.f90
    error_handler.f90
    mp.f90
    mp_base.f90
    nvtx_wrapper.f90
    parallel_include.f90
    util_param.f90
    # GPU
    mp_base_gpu.f90
    mp_wave.f90)
qe_enable_cuda_fortran("${src_util}") 
qe_add_library(qeMPLib ${src_util} "cptimer.c" )

target_link_libraries(qeMPLib
    PUBLIC
        qe_ext_prof_tool # to pass link options, PUBLIC is needed.
    PRIVATE
        qe_openmp_fortran
        qe_mpi_fortran)




