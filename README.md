# qeMPlib

provides few APIs for MPI calls, timing and error handling in QE miniapps 

## Installation with CMake with internal FFTS 

*  create a build directory and cd into it 
```
mkdir build 
cd build 
``` 

* use cmake command to create your build envinronment.
``` 
cmake  ../ -DCMAKE_INSTALL_PREFIX=<install-path>  -DQE_ENABLE_OPENMP=1
```
* Compile the library and install it with make:  
```
make 
make install 
```


# To do list 
* include GPU support NVidia, AMD, GPU 
* extend cmake 

